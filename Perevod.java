//Напишите программу, которая будет переводить секунды в форматированный вид - часы минуты секунды
//Примеры:
//1249
//20 минут 49 секунд
//648958
//180 часов 15 минут 58 секунд
//
//
package SOcencoy;

import java.util.Scanner;

public class Perevod {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число");
        int cek = scanner.nextInt();
        //Вторая строка
        int c = cek / 3600;
        int d = (cek % 3600) / 60;
        int e = (cek % 3600) % 60;
        //Часы
        if ((c % 100 >= 11 && c % 100 <= 19) || (c % 10 >= 5) || ((c % 10 == 0) && c != 0)) {
            System.out.print(" " + c + " часов");
        } else if (c % 10 == 1) {
            System.out.print(" " + c + " час");
        } else if ((c % 10 >= 2) && (c % 10 <= 4)) {
            System.out.print(" " + c + " часа");
        }
        //Минуты
        if ((d >= 11 && d <= 19)||(d % 10 >= 5)||((d % 10 == 0) && d != 0)) {
            System.out.print(" " + d + " минут");
        }  else if (d % 10 == 1) {
            System.out.print(" " + d + " минута");
        }  else if ((d % 10 >= 2) && (d % 10 <= 4)) {
            System.out.print(" " + d + " минуты");
        }
        //Секунды
        if ((e >= 11 && e <= 19)||(e % 10 >= 5)||((e % 10 == 0) && e != 0)) {
            System.out.print(" " + e + " секунд");
        }  else if (e % 10 == 1) {
            System.out.print(" " + e + " секунда");
        } else if ((e % 10 >= 2) && (e % 10 <= 4)) {
            System.out.print(" " + e + " секунды");
        }
        System.out.println(" ");
        //Третья строка
        int f = cek / (7 * 24 * 3600);
        int g = cek % (7 * 24 * 3600) / (24 * 3600);
        int h = cek % (7 * 24 * 3600) % (24 * 3600) / 3600;
        int j = cek % (7 * 24 * 3600) % (24 * 3600) % 3600 / 60;
        int k = cek % (7 * 24 * 3600) % (24 * 3600) % 3600 % 60;
        //Неделя
        if ((f % 100 >= 11 && f % 100 <= 19)||(f % 10 >= 5)||((f % 10 == 0) && f != 0)) {
            System.out.print(" " + f + " недель");
        }  else if (f % 10 == 1) {
            System.out.print(" " + f + " неделя");
        }  else if ((f % 10 >= 2) && (f % 10 <= 4)) {
            System.out.print(" " + f + " недели");
        }
        //Сутки
        if (g >= 2) {
            System.out.print(" " + g + " суток");
        } else if (g == 1) {
            System.out.print(" " + g + "cутки");
        }
        //Часы
        if (h >= 5 && h <= 20) {
            System.out.print(" " + h + " часов");
        } else if (h % 10 == 1) {
            System.out.print(" " + h + " час");
        } else if (h % 10 >= 2) {
            System.out.print(" " + h + " часа");
        }
        //Минуты
        if ((j >= 11 && j <= 19)||(j % 10 >= 5)||((j % 10 == 0) && j != 0)) {
            System.out.print(" " + j + " минут");
        }  else if (j % 10 == 1) {
            System.out.print(" " + j + " минута");
        }  else if ((j % 10 >= 2) && (j % 10 <= 4)) {
            System.out.print(" " + j + " минуты");
        }
        //Секунды
        if ((k >= 11 && k <= 19)||(k % 10 >= 5)||((k % 10 == 0) && k != 0)) {
            System.out.print(" " + k + " секунд");
        }  else if (k % 10 == 1) {
            System.out.print(" " + k + " секунда");
        }  else if ((k % 10 >= 2) && (k % 10 <= 4)) {
            System.out.print(" " + k + " секунды");
        }
    }
}