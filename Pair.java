package jc;
/*
Реализовать класс Pair, который будет создержать 2 значения любого типа.
Класс умеет выводить:
first() - возвращает 1ый элемент
last() - возвращает 2ой элемент
swap() - меняет элементы местами
replaceFirst() - заменяет 1ый элемент на новый
replaceLast() - заменяет 2ой элемент на новый*/
public class Pair<T> {
    private T a;
     private T b ;
    Pair(T a, T b) {
        this.a = a;
        this.b = b;
    }
    public T first() {
         T first = a;
        System.out.println("Первый элемент " + first);
        return first;
    }
    public T second() {
        T second = b;
        System.out.println("Второй элемент " + second);
        return second;
    }
    public void swap() {
        T c ;
        c = a;
        a = b;
        b = c;
    }
    public T replaceFirst(T c) {
         a =c;
        return first();
    }
    public T replaceLast(T d) {
        b = d;
        return second();
    }
    public static void main(String[] args) {
        Pair pair = new Pair(4, "d");
        pair.first();
        pair.second();
        pair.swap();
        pair.first();
        pair.second();
        pair.replaceFirst(-1);
        pair.replaceLast(124.564);

    }
}

