/*Имеется строка с текстом.
Подсчитайте уникальное количество слов в тексте.
Учитывайте, что слова могут разделяться несколькими пробелами, знаками препинания.
Пробелы могут присутствовать в начале и конце текста.
Примеры уникальности:

dog и Dog - одинаковые
dog и dogs - разные*/
package SOcencoy;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class OriginalWorld {
    public static void main(String[] args) {
        Set<String> words = new HashSet<String>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите текст");
        String input = scanner.nextLine();
        input=input.toLowerCase();
        Pattern pattern = Pattern.compile("([A-z])\\w{1,}");
        Matcher matcher = pattern.matcher(input);
        while (matcher.find()) {
            words.add(matcher.group());
        }
        System.out.println("Число уникальных слов:"+words.size());
    }
}
