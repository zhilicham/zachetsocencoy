//Создать двумерный квадратный массив размера n.
//Создать двумерный квадратный массив размера n.
//Заполнить его случайными числами таким образом:
//
//числа по диагонали равны 0;
//вверху и снизу от пересечения диагоналей находятся только положительные числа;
//слева и справа от пересечения диагоналей находятся только отрицательные числа;
//
//Вывести массив на экран.
//
//Найти сумму всех элементов
//Найти среднее арифметическое всех элементов, которые больше суммы всех элементов
//
//
//Пример:
// 0  4  5  5  3  8  0
//-2  0  5  6  3  0 -4
//-2 -8  0  6  0 -1 -2
//-5 -9 -2  0 -9 -7 -3
//-6 -2  0  1  0 -1 -5
//-2  0  3  5  2  0 -8
// 0  4  2  9  1  3  0
//
//Суммa всех элементов: -3
//Cреднее арифметическое всех элементов больше -3: 1.6579
package SOcencoy;

import java.util.Scanner;

public class Array {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длину массива.");
        int n = scanner.nextInt();
        int[][] z = new int[n][n];
        //Заполнение
        for (int i = 0; i < z.length; i++) {
            for (int j = 0; j < z[i].length; j++) {
                z[i][j] = (int) (Math.random() * -9 - 1);
                // Диагонали
                if ((i == j) || (j == (z.length - i - 1))) {
                    z[i][j] = 0;
                }
            }
            //Верх от диагоналей
            for (int j = i + 1; j < z[i].length - i - 1; j++) {
                z[i][j] = (int) (Math.random() * 9 + 1);
            }
            //Низ от диагоналей
            for (int j = z.length - i; j < i; j++) {
                z[i][j] = (int) (Math.random() * 9 + 1);
            }
        }
        /*//Верх от диагоналей
        for (int i = 0; i < z.length; i++) {
            for (int j = i + 1; j < z[i].length - i - 1; j++) {
                z[i][j] = (int) (Math.random() * 9 + 1);

            }
        }
        //Низ от диагоналей
        for (int i = z.length - 1; i > 0; i--) {
            for (int j = z.length - i; j < i; j++) {
                z[i][j] = (int) (Math.random() * 9 + 1);
            }
        }
       //Дагональ
        for (int i = 0; i < z.length; i++) {
            for (int j = 0; j < z[i].length; j++) {
                //Первая диаголаль
                if (i == j) {
                    z[i][j] = 0;
                }
                //Вторая диаголаль
                if (j == (z.length - i - 1)) {
                    z[i][j] = 0;
                }
            }
        }*/
        //Вывод массива
        for (int[] a : z) {
            for (int b : a) {
                if (b >= 0) {
                    System.out.print("  " + b);

                } else {
                    System.out.print(" " + b);
                }
            }
            System.out.println();
        }
        //Сумма всех элементов
        int sum = 0;
        for (int i = 0; i < z.length; i++) {
            for (int j = 0; j < z[i].length; j++) {
                sum += z[i][j];
            }
        }
        System.out.println("Сумма всех элементов: " + sum + ".");
        //Cреднее
        int k = 0;
        double sr = 0;
        for (int i = 0; i < z.length; i++) {
            for (int j = 0; j < z[i].length; j++) {
                if ((z[i][j]) > sum) {
                    k = k + 1;
                    sr = sr + z[i][j];
                }
            }
        }
        sr = sr / k;
        if (k == 0) {
            System.out.println("В массиве нет элементов больше " + sum);
        } else {
            System.out.println("Среднее арифметическое всех элементов больше " + sum + ":" + " " + sr);
        }
    }
}


